# Create Readme 
[npm-create-readme](https://bitbucket.org/tizdev/npm-create-readme/src/master/)

---

- Node.js 10.15.3
- Status `DEV`

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Contributors](#markdown-header-contributors)
    * [Description](#markdown-header-description)
    * [Plugins](#markdown-header-plugins)
    * [Environments](#markdown-header-environments)
* [Getting started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Installation](#markdown-header-installation)
    * [Credentials](#markdown-header-credentials)
* [Test and deploy](#markdown-header-test-and-deploy)
    * [Use in local](#markdown-header-use-in-local) 
    * [Install the script from npm](#markdown-header-install-the-script-from-npm) 
    * [Run](#markdown-header-run) 
    * [Deploy](#markdown-header-deploy) 
  

---

## About the project
### Contributors

 * Developers : Alexis BARDONNET
 * Designer : /
 * Project manager : /
 * Marketing : /
 * Commercial : /
 

### Description

With this module, You can directly download a conventional README for your projects.

### Plugins

 * node-fetch 2.6.1

 
### Environments

 * Prod : https://www.npmjs.com/package/create-readme-tiz

---

## Getting started
### Prerequisites

 * Node.js

https://nodejs.org/en/download/

 * npm

```sh
$ npm install npm@latest -g
```

### Installation

1. Install dependencies
```sh
$ npm install 
```

### Credentials
No credentials needed

---

## Test and deploy

### Use in local

To use the script in local (from this project repository), run :

```sh
$ npm link
```

### Install the script from npm

To install the script from npm, run :

```sh
$ sudo npm install -g create-readme-tiz
```

### Run

To run the script, use :

```sh
$ readme [key]
```

Values of [key] :

 * drupal
 * flutter
 * laravel
 * prestashop
 * symfony
 * reactjs
 * standard
 * wordpress
 * xamarin


### Deploy

To deploy the plugin, logging as Tiz npm user (available in lastpass), and run the command :

```sh
$ npm publish .
```

Don't forget to update the version number in `package.json`.
